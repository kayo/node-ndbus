# Overview

This is a DBus integration addon for [NodeJS](http://nodejs.org/). It developed for performance and stability, ease of using and supporting.

It uses a [libdbus](http://www.freedesktop.org/wiki/Software/dbus/) only and doesn't depends from other libraries, like [GLib](http://library.gnome.org/devel/glib/) and etc.

## Why?

When I needed to use DBus in my projects, I tried to use all found DBus modules, but nothing of it worked properly for me.

-   [DBus addon by Shogun](https://github.com/Shouqun/node-dbus) has strange and inconvenient API.

Asynchronous operations fails to work for me. Also I cannot receive signals and implement service method calls.
In early stage I tries to fix that behavior, but internal implementation looks like too tricky to support it.

-   [DBus addon by Motorola-Mobility](https://github.com/Motorola-Mobility/node-dbus) is too low-level implementation, in fact it's almost a libdbus bindings.

So high-level wrapper required to make it more convenient and easy for use.
I tried to launch with it, but I don't agree what stuff like manipulating with DBus messages on JS-side is a good idea.

-   [Native implementation by Sidorares](https://github.com/sidorares/node-dbus) works more properly, but it have some serious problems with introspection and marshaling.

Of course, using native implementation also is a not good idea for performance reasons.

## How?

Internally this addon uses libuv for handling incoming data, so all interaction occurs in same thread with node and [v8](https://code.google.com/p/v8/).

# Installation

    npm install ndbus

# Usage

    var DBus = require('ndbus');
    ...
    
    var session_bus = DBus();
    var system_bus = DBus(true);
    var custom_bus = DBus("custom_bus_name");

## High-Level API

### Interfacing with proxies

Objects must be introspectable to be able interact with its through proxies.

    // binding object through proxy
    bus.proxy("org.awesome.Service", "/path/to/object", "org.awesome.Interface",
    function(err, some_object){
      // we need to handle errors
      ...
      // calling method of proxied object
      some_object.SomeMethod(arg0, arg1, ..., argN, function(err, ret0, ret1, ..., retN){
        // we need to handle errors
        ...
      });
      // binding to signal of proxied object
      function someListener(arg0, arg1, ..., argN){
        ...
      }
      some_object.SomeSignal.on(someListener);
      // unbinding from signal of proxied object
      some_object.SomeSignal.off(someListener);
      // binding to signal of proxied object only for one time
      some_object.SomeSignal.once(someListener);
      ...
      // removing proxy
      some_object.$();
    });

### Creating services

To create service you need to provide specification in simplified JSON-like format.

    // creating service on the bus
    bus.service("org.awesome.Service", function(err, service){
      // we need to handle errors
      ...
      // creating node with callback
      service.node("/path/to/object", function(err, node){
        // providing interface with callback
        node.iface("org.awesome.Interface", {
          // interface specification
          method: {
            // methods
          },
          signal: {
            // signals
          },
          attrib: {
            // attributes (properties)
          },
          $: {
            // annotations
          }
        }, function(err, iface){
          // we need to handle errors
          ...
          // implementing method
          iface.SomeMethod(function(arg0, arg1, ..., argN, result){
            ...
            // emiting signal
            iface.SomeSignal(arg0, arg1, ..., argN);
          });
        });
        // providing interface with callback and object
        node.iface("org.awesome.Interface", {
          // interface specification
          ...
        }, {
          SomeMethod: function(arg0, arg1, ..., argN, result){
            ...
            // emiting signal
            this.SomeSignal(arg0, arg1, ..., argN);
          }
        }, function(err, iface){
          // we need to handle errors
          ...
          // removing interface
          iface.$();
        });
        ...
        // deleting node
        node.$();
      });
      ...
      // deleting service
      service.$();
    });

## Low-Level API

You need to use this only for extending High-Level API and in some special cases.

    var nDBus = require('ndbus').internal;
    ...
    var some_connection = nDBus.Connection(); // like DBus()
    var some_interface = nDBus.Interface(some_connection); // like bus.proxy()
    var some_method = nDBus.Method(some_interface, "MethodName", "in signature", "out signature");
    var some_signal = nDBus.Signal(some_interface, "SignalName", "signature");
    ...
    // call method
    some_method.call([arg0, arg1, ..., argN], function(err, returns){
      var ret0 = returns[0], ret1 = returns[1], ..., retN = returns[N];
      ...
    });
    // register method callback
    some_method.reg(function(args, result){
      var arg0 = args[0], arg1 = args[1], ..., argN = args[N];
      ...
      // asynchronous return case
      result([ret0, ret1, ..., retN]);
      // synchronous return case
      return [ret0, ret1, ];
    });
    // get registered method callback
    var callback = some_method.reg();
    // unregister method callback
    some_method.reg(null);
    ...
    // emit signal
    some_signal.emit([arg0, arg1, ..., argN]);
    // register signal listener
    some_signal.reg(function(args){
      var arg0 = args[0], arg1 = args[1], ..., argN = args[N];
      ...
    });
    // get registered signal listener
    var listener = some_signal.reg();
    // unregister signal listener
    some_signal.reg(null);

# Licensing

This addon are available under GNU General Public License version 3.

ndbus — DBus integration addon for [NodeJS](http://nodejs.org/).
Copyright © 2013  Kayo Phoenix <kayo@illumium.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
