{
  'targets': [{
    'target_name': 'ndbus',
    'defines': [
      'DIE_ON_EXCEPTION'
    ],
    'sources': [
      'src/common.cc',
      'src/message.cc',
      'src/incoming.cc',
      'src/outgoing.cc',
      'src/connection.cc',
      'src/interface.cc',
      'src/method.cc',
      'src/signal.cc',
      'src/addon.cc'
    ],
    'conditions': [
      ['OS=="linux"', {
        'defines': [
        ],
        'cflags': [
          '-Wall',
          '<!@(pkg-config --cflags dbus-1)'
        ],
        'ldflags': [
          '<!@(pkg-config  --libs-only-L --libs-only-other dbus-1)'
        ],
        'libraries': [
          '<!@(pkg-config  --libs-only-l --libs-only-other dbus-1)'
        ]
      }]
    ]
  }]
}
