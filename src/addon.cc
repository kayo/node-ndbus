#include "common.hh"
#include "connection.hh"
#include "interface.hh"
#include "method.hh"
#include "signal.hh"

namespace ndbus {
  extern "C" void
  init (Handle<Object> exports) {
    HandleScope scope;
    
    Common::Init(exports);
    Connection::Init(exports);
    Interface::Init(exports);
    Method::Init(exports);
    Signal::Init(exports);
  }
}

NODE_MODULE(ndbus, ndbus::init);
