#include "common.hh"

namespace ndbus {
  
  Handle<Value>
  Common::GetVersion(const Arguments& args){
    HandleScope scope;
    
    int major, minor, micro;
    
    dbus_get_version(&major, &minor, &micro);
    
    Local<Array> version = Array::New(3);
    
    version->Set(0, Number::New(major));
    version->Set(1, Number::New(minor));
    version->Set(2, Number::New(micro));

    return scope.Close(version);
  }

  Handle<Value>
  Common::GetMachine(const Arguments& args){
    HandleScope scope;
    
    char *id = dbus_get_local_machine_id();
    
    Local<String> machine = String::New(id);
    
    dbus_free(id);
    
    return scope.Close(machine);
  }
  
  void
  Common::Init(Handle<Object> target){
    SetMethod(target, "Version", GetVersion);
    SetMethod(target, "Machine", GetMachine);
    
    SetMethod(target, "Validate", Validate);
  }

  Handle<Value>
  Common::Validate(const Arguments& args) {
    HandleScope scope;
    
    if(args.Length() < 2 || !args[0]->IsString() || !args[1]->IsString()){
      return scope.Close(Boolean::New(true));
    }
    
    String::Utf8Value type(args[0]->ToString());
    String::Utf8Value value(args[1]->ToString());
    DBusErrorWrap dbus_err;
    dbus_bool_t status = false;
    
    switch((*type)[0]){
    case 'b': // bus or service name
      status = dbus_validate_bus_name(*value, &dbus_err);
      break;
    case 'p': // node or object path
      status = dbus_validate_path(*value, &dbus_err);
      break;
    case 'i': // interface name
      status = dbus_validate_interface(*value, &dbus_err);
      break;
    case 'm': // member name
      status = dbus_validate_member(*value, &dbus_err);
      break;
    case 's': // signature
      status = dbus_signature_validate_single(*value, &dbus_err);
      break;
    case 'S': // signatures
      status = dbus_signature_validate(*value, &dbus_err);
      break;
    }
    
    if(!status){
      return scope.Close(String::New(*dbus_err));
    }
    
    return scope.Close(Boolean::New(false));
  }
  
  Handle<Value>
  callbackError(Handle<Function> callback, Handle<Value> error){
    //HandleScope scope;
    
    Local<Context> context = Context::GetCurrent();
    
    Handle<Value> args[1] = {
      error
    };
    
    TryCatch try_catch;
    
    Handle<Value> ret = callback->Call(context->Global(), 1, args);
    
    HANDLE_CAUGHT(try_catch);
    
    //return scope.Close(ret);
    return ret;
  }
}
