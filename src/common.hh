#ifndef __COMMON_HH__
#define __COMMON_HH__

extern "C" {
#include <dbus/dbus.h>
}

#include <v8.h>
#include <node.h>
#include <node_version.h>
#include <node_object_wrap.h>
#include <uv.h>

#include "debug.hh"

#include <string>
#include <sstream>

namespace ndbus {
  using namespace std;
  using namespace v8;
  using namespace node;
  
  class Common {
  public:
    static void Init(Handle<Object> target);

    static Handle<Value> GetVersion(const Arguments& args);
    static Handle<Value> GetMachine(const Arguments& args);
    
    static Handle<Value> Validate(const Arguments& args);
  };
  
  Handle<Value>
  callbackError(Handle<Function> callback,
                Handle<Value> error);

  class DBusErrorWrap {
  protected:
    DBusError dbus_error;
  public:
    DBusErrorWrap(){
      dbus_error_init(&dbus_error);
    }
    ~DBusErrorWrap(){
      dbus_error_free(&dbus_error);
    }
    DBusError *operator&(){
      return &dbus_error;
    }
    const char *operator*() const{
      return (const char*)(*this);
    }
    operator bool() const{
      return dbus_error_is_set(&dbus_error);
    }
    operator const char*() const{
      return (*this) ? dbus_error.message : "";
    }
  };
  
#define EXCEPTION(_type_, _msg_) Exception::_type_(String::New(_msg_))
#define THROW_ERROR(_type_, _msg_) ThrowException(EXCEPTION(_type_, _msg_))
#define RET_ERROR(_type_, _msg_) return THROW_ERROR(_type_, _msg_)
#define THROW_SCOPE(_type_, _msg_) return scope.Close(THROW_ERROR(_type_, _msg_))
#define CALL_ERROR_(_cb_, _type_, _msg_) callbackError(_cb_, EXCEPTION(_type_, _msg_))
#define CALL_ERROR(_type_, _msg_) CALL_ERROR_(callback, _type_, _msg_)

#ifdef DIE_ON_EXCEPTION
#define HANDLE_CAUGHT(_try_)                    \
  if(_try_.HasCaught()){                        \
    FatalException(_try_);                      \
  }
#else
#define HANDLE_CAUGHT(_try_)                    \
  if(_try_.HasCaught()){                        \
    DisplayExceptionLine(_try_);                \
  }
#endif

}

#endif//__COMMON_HH__
