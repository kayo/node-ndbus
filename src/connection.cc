#include "connection.hh"

#include <map>

namespace ndbus {
  typedef map<DBusConnection*,Connection*> ConnectionMap;

  ConnectionMap conn_map;
  
  void
  Connection::Init(Handle<Object> target){
    Local<FunctionTemplate> tpl = FunctionTemplate::New(Construct);
    
    tpl->SetClassName(String::NewSymbol("DBusConnection"));
    tpl->InstanceTemplate()->SetInternalFieldCount(1);

    SetPrototypeMethod(tpl, "id", GetId);
    
    target->Set(String::NewSymbol("Connection"), tpl->GetFunction());
  }

  Handle<Value>
  Connection::Construct(const Arguments& args) {
    HandleScope scope;
    
    DBusBusType type = DBUS_BUS_SESSION;
    DBusConnection *dbus_conn = NULL;
    
    string addr;
    
    if(args.Length() > 0){
      if(args[0]->IsBoolean()){
        type = args[0]->BooleanValue() ? DBUS_BUS_SYSTEM : DBUS_BUS_SESSION;
      }else if(args[0]->IsString()){
        String::Utf8Value str(args[0]->ToString());
        addr = *str;
      }else if(args[0]->IsNull()){
        type = DBUS_BUS_STARTER;
      }
    }
    
    /* get connection by type */
    DBusErrorWrap dbus_err;
    
    if(addr.length()){
      dbus_conn = dbus_connection_open(addr.c_str(), &dbus_err);
    }else{
      dbus_conn = dbus_bus_get(type, &dbus_err);
    }
    
    if(dbus_err){
      string msg("Connection to bus failed: ");
      msg += *dbus_err;
      
      THROW_SCOPE(Error, msg.c_str());
    }
    
    ConnectionMap::iterator iter = conn_map.find(dbus_conn);
    
    if(iter != conn_map.end()){
      /* already created */
      return scope.Close(iter->second->handle_);
    }else{
      /* initialize instance */
      Connection *conn = new Connection(dbus_conn);
      conn->Wrap(args.This());
      
      return scope.Close(args.This());
    }
  }

  Handle<Value>
  Connection::GetId(const Arguments& args){
    HandleScope scope;
    
    Connection *conn = ObjectWrap::Unwrap<Connection>(args.This());

    if(!conn){
      THROW_SCOPE(Error, "Connection object corrupted.");
    }
    
    string val(conn->id());
    
    return scope.Close(String::New(val.c_str()));
  }
  
  Connection::Connection(DBusConnection *conn): dbus_conn(conn){
    LOG("* %s", O2P(*this));
    poll_start();
    conn_map[dbus_conn] = this;
  }
  
  Connection::~Connection(){
    LOG("~ %s", O2P(*this));
    conn_map.erase(dbus_conn);
    poll_stop();
    dbus_connection_unref(dbus_conn);
  }
  
  string
  Connection::id(){
    DBusErrorWrap dbus_err;
    
    return dbus_bus_get_id(dbus_conn, &dbus_err);
  }

/*
 * UV polling
 */
  
  static void
  poll_incoming(DBusConnection* dbus_conn){
    if(dbus_connection_read_write(dbus_conn, 0)){
      for(DBusDispatchStatus status = dbus_connection_get_dispatch_status(dbus_conn);
          status == DBUS_DISPATCH_DATA_REMAINS;
          status = dbus_connection_dispatch(dbus_conn)){
        LOG("i/o");
      }
    }else{
      LOG("close");
    }
  }
  
  static void
  poll_handler(uv_poll_t* handle, int status, int events){
    if(status < 0){
      return;
    }
    
    DBusConnection* dbus_conn = (DBusConnection*)handle->data;
    
    if(events & UV_READABLE){ /* we have an incoming message */
      poll_incoming(dbus_conn);
    }
  }
  
  void
  Connection::poll_start(){
    uv_poll.data = (void*)dbus_conn;
    
    uv_loop_t* loop = uv_default_loop();
    
    int fd;
    if(dbus_connection_get_socket(dbus_conn, &fd)){
      uv_poll_init_socket(loop, &uv_poll, fd);
    }else{
      if(dbus_connection_get_unix_fd(dbus_conn, &fd)){
        uv_poll_init(loop, &uv_poll, fd);
      }
    }
    
    uv_poll_start(&uv_poll, UV_READABLE, poll_handler);
  }
  
  void
  Connection::poll_stop(){
    uv_poll_stop(&uv_poll);
  }
}
