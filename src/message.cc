#include "message.hh"

namespace ndbus {
  Message::Message()
    : dbus_sign(NULL), dbus_mesg(NULL) {
    //LOG("create %s", O2P(*this));
  }

  Message::Message(DBusMessage *mesg)
    : dbus_sign(NULL), dbus_mesg(mesg) {
    //LOG("create %s", O2P(*this));
    dbus_message_ref(dbus_mesg);
  }
  
  Message::~Message(){
    //LOG("delete %s", O2P(*this));
    dbus_message_unref(dbus_mesg);
  }

  int
  Message::sign_count()const{
    if(!dbus_sign){
      return -1;
    }
    
    DBusSignatureIter sign_iter;
    int count;
    
    for(count = 0, dbus_signature_iter_init(&sign_iter, dbus_sign);
        dbus_signature_iter_get_current_type(&sign_iter) != DBUS_TYPE_INVALID;
        count++, dbus_signature_iter_next(&sign_iter));
    
    return count;
  }

  int
  Message::mesg_count()const{
    if(!dbus_mesg){
      return -1;
    }
    
    DBusMessageIter mesg_iter;
    int count;
    
    for(count = 0, dbus_message_iter_init(dbus_mesg, &mesg_iter);
        dbus_message_iter_get_arg_type(&mesg_iter) != DBUS_TYPE_INVALID;
        count++, dbus_message_iter_next(&mesg_iter));

    return count;
  }
}
