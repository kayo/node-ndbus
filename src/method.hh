#ifndef __METHOD_HH__
#define __METHOD_HH__

#include "common.hh"
#include "message.hh"
#include "interface.hh"

namespace ndbus{
  class Method: public Member {
    friend class Pending;
  protected:
    string rule();
    //bool on(DBusMessage *msg);
    
    bool dispatch(DBusMessage *dbus_mesg);
  public:
    string arg;
    string res;
    
    Method(Interface& interface, string name, string arg, string res);
    ~Method();
    
    static void Init(Handle<Object> target);
    static Handle<Value> Construct(const Arguments& args);
    
    static Handle<Value> DoCall(const Arguments& args);
    static Handle<Value> OnCall(const Arguments& args);

    static Handle<Value> OnRes(const Arguments& args);
    static Persistent<ObjectTemplate> OnResData;

    O2S("method [%p] %s <%s >%s | %s", this, name.c_str(), arg.c_str(), res.c_str(), O2P(interface));
  };
  
  class Call: public Outgoing {
    friend class Method;
    friend class Pending;
  protected:
    Method& method;
    Persistent<Function> callback;
    int timeout;
    
    Call(Method& method);
    ~Call();
    
    Handle<Value> call();
    
    static void OnResult(DBusPendingCall *dbus_pend, void *self);
    static void OnClean(void *self);
    
    O2S("call [%p] | %s", this, O2P(method));
  };

  class Pending {
    friend class Call;
  protected:
    Method& method;
    Persistent<Function> callback;
    
    Pending(Method &method, Handle<Function> callback);
    ~Pending();
    
    O2S("pending [%p] | %s", this, O2P(method));
  };
  
  class Return: public Incoming {
    friend class Call;
  protected:
    Method& method;
    
    Return(Method& method, DBusPendingCall *dbus_pend);
    ~Return();
    
    O2S("return [%p] of %s", this, O2P(method));
  };

  class Args: public Incoming {
    friend class Method;
  protected:
    Method& method;
    
    Args(Method& method, DBusMessage *mesg);
    ~Args();

    O2S("args [%p]", this);
  };
  
  class Result: public Outgoing {
    friend class Method;
  protected:
    Method& method;
    
    Result(Method& method, DBusMessage *call);
    ~Result();

    Handle<Value> send();
    
    O2S("result [%p] | %s", this, O2P(method));
  };
}

#endif//__METHOD_HH__
