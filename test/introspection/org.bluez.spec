{
  "": {
    "org.freedesktop.DBus.Introspectable": {
      "method": {
        "Introspect": {
          "res": {
            "xml": "s"
          }
        }
      }
    },
    "org.bluez.Manager": {
      "method": {
        "GetProperties": {
          "res": {
            "properties": "a{sv}"
          }
        },
        "DefaultAdapter": {
          "res": {
            "adapter": "o"
          }
        },
        "FindAdapter": {
          "arg": {
            "pattern": "s"
          },
          "res": {
            "adapter": "o"
          }
        },
        "ListAdapters": {
          "res": {
            "adapters": "ao"
          }
        }
      },
      "signal": {
        "PropertyChanged": {
          "arg": {
            "name": "s",
            "value": "v"
          }
        },
        "AdapterAdded": {
          "arg": {
            "adapter": "o"
          }
        },
        "AdapterRemoved": {
          "arg": {
            "adapter": "o"
          }
        },
        "DefaultAdapterChanged": {
          "arg": {
            "adapter": "o"
          }
        }
      }
    }
  },
  "org": {}
}
