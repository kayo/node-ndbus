{
  "": {
    "org.freedesktop.DBus.Introspectable": {
      "method": {
        "Introspect": {
          "res": {
            "xml": "s"
          }
        }
      }
    },
    "org.ofono.Modem": {
      "method": {
        "GetProperties": {
          "res": {
            "properties": "a{sv}"
          }
        },
        "SetProperty": {
          "arg": {
            "property": "s",
            "value": "v"
          }
        }
      },
      "signal": {
        "PropertyChanged": {
          "arg": {
            "name": "s",
            "value": "v"
          }
        }
      }
    },
    "org.ofono.SimManager": {
      "method": {
        "GetProperties": {
          "res": {
            "properties": "a{sv}"
          }
        },
        "SetProperty": {
          "arg": {
            "property": "s",
            "value": "v"
          }
        },
        "ChangePin": {
          "arg": {
            "type": "s",
            "oldpin": "s",
            "newpin": "s"
          }
        },
        "EnterPin": {
          "arg": {
            "type": "s",
            "pin": "s"
          }
        },
        "ResetPin": {
          "arg": {
            "type": "s",
            "puk": "s",
            "newpin": "s"
          }
        },
        "LockPin": {
          "arg": {
            "type": "s",
            "pin": "s"
          }
        },
        "UnlockPin": {
          "arg": {
            "type": "s",
            "pin": "s"
          }
        },
        "GetIcon": {
          "arg": {
            "id": "y"
          },
          "res": {
            "icon": "ay"
          }
        }
      },
      "signal": {
        "PropertyChanged": {
          "arg": {
            "name": "s",
            "value": "v"
          }
        }
      }
    },
    "org.ofono.MessageWaiting": {
      "method": {
        "GetProperties": {
          "res": {
            "properties": "a{sv}"
          }
        },
        "SetProperty": {
          "arg": {
            "property": "s",
            "value": "v"
          }
        }
      },
      "signal": {
        "PropertyChanged": {
          "arg": {
            "name": "s",
            "value": "v"
          }
        }
      }
    },
    "org.ofono.CallForwarding": {
      "method": {
        "GetProperties": {
          "res": {
            "properties": "a{sv}"
          }
        },
        "SetProperty": {
          "arg": {
            "property": "s",
            "value": "v"
          }
        },
        "DisableAll": {
          "arg": {
            "type": "s"
          }
        }
      },
      "signal": {
        "PropertyChanged": {
          "arg": {
            "name": "s",
            "value": "v"
          }
        }
      }
    },
    "org.ofono.CallSettings": {
      "method": {
        "GetProperties": {
          "res": {
            "properties": "a{sv}"
          }
        },
        "SetProperty": {
          "arg": {
            "property": "s",
            "value": "v"
          }
        }
      },
      "signal": {
        "PropertyChanged": {
          "arg": {
            "property": "s",
            "value": "v"
          }
        }
      }
    },
    "org.ofono.CallBarring": {
      "method": {
        "GetProperties": {
          "res": {
            "properties": "a{sv}"
          }
        },
        "SetProperty": {
          "arg": {
            "property": "s",
            "value": "v",
            "pin2": "s"
          }
        },
        "DisableAll": {
          "arg": {
            "password": "s"
          }
        },
        "DisableAllIncoming": {
          "arg": {
            "password": "s"
          }
        },
        "DisableAllOutgoing": {
          "arg": {
            "password": "s"
          }
        },
        "ChangePassword": {
          "arg": {
            "old": "s",
            "new": "s"
          }
        }
      },
      "signal": {
        "PropertyChanged": {
          "arg": {
            "name": "s",
            "value": "v"
          }
        }
      }
    },
    "org.ofono.VoiceCallManager": {
      "method": {
        "GetProperties": {
          "res": {
            "properties": "a{sv}"
          }
        },
        "Dial": {
          "arg": {
            "number": "s",
            "hide_callerid": "s"
          },
          "res": {
            "path": "o"
          }
        },
        "Transfer": {},
        "SwapCalls": {},
        "ReleaseAndAnswer": {},
        "HoldAndAnswer": {},
        "HangupAll": {},
        "PrivateChat": {
          "arg": {
            "call": "o"
          },
          "res": {
            "calls": "ao"
          }
        },
        "CreateMultiparty": {
          "res": {
            "calls": "o"
          }
        },
        "HangupMultiparty": {},
        "SendTones": {
          "arg": {
            "SendTones": "s"
          }
        },
        "GetCalls": {
          "res": {
            "calls_with_properties": "a(oa{sv})"
          }
        }
      },
      "signal": {
        "Forwarded": {
          "arg": {
            "type": "s"
          }
        },
        "BarringActive": {
          "arg": {
            "type": "s"
          }
        },
        "PropertyChanged": {
          "arg": {
            "name": "s",
            "value": "v"
          }
        },
        "CallAdded": {
          "arg": {
            "path": "o",
            "properties": "a{sv}"
          }
        },
        "CallRemoved": {
          "arg": {
            "path": "o"
          }
        }
      }
    },
    "org.ofono.AudioSettings": {
      "method": {
        "GetProperties": {
          "res": {
            "properties": "a{sv}"
          }
        }
      },
      "signal": {
        "PropertyChanged": {
          "arg": {
            "name": "s",
            "value": "v"
          }
        }
      }
    },
    "org.ofono.RadioSettings": {
      "method": {
        "GetProperties": {
          "res": {
            "properties": "a{sv}"
          }
        },
        "SetProperty": {
          "arg": {
            "property": "s",
            "value": "v"
          }
        }
      },
      "signal": {
        "PropertyChanged": {
          "arg": {
            "name": "s",
            "value": "v"
          }
        }
      }
    },
    "org.ofono.SupplementaryServices": {
      "method": {
        "Initiate": {
          "arg": {
            "command": "s"
          },
          "res": {
            "result_name": "s",
            "value": "v"
          }
        },
        "Respond": {
          "arg": {
            "reply": "s"
          },
          "res": {
            "result": "s"
          }
        },
        "Cancel": {},
        "GetProperties": {
          "res": {
            "properties": "a{sv}"
          }
        }
      },
      "signal": {
        "NotificationReceived": {
          "arg": {
            "message": "s"
          }
        },
        "RequestReceived": {
          "arg": {
            "message": "s"
          }
        },
        "PropertyChanged": {
          "arg": {
            "name": "s",
            "value": "v"
          }
        }
      }
    },
    "org.ofono.Phonebook": {
      "method": {
        "Import": {
          "res": {
            "entries": "s"
          }
        }
      }
    },
    "org.ofono.NetworkRegistration": {
      "method": {
        "GetProperties": {
          "res": {
            "properties": "a{sv}"
          }
        },
        "Register": {},
        "GetOperators": {
          "res": {
            "operators_with_properties": "a(oa{sv})"
          }
        },
        "Scan": {
          "res": {
            "operators_with_properties": "a(oa{sv})"
          }
        }
      },
      "signal": {
        "PropertyChanged": {
          "arg": {
            "name": "s",
            "value": "v"
          }
        }
      }
    },
    "org.ofono.CellBroadcast": {
      "method": {
        "GetProperties": {
          "res": {
            "properties": "a{sv}"
          }
        },
        "SetProperty": {
          "arg": {
            "property": "s",
            "value": "v"
          }
        }
      },
      "signal": {
        "PropertyChanged": {
          "arg": {
            "property": "s",
            "value": "v"
          }
        },
        "IncomingBroadcast": {
          "arg": {
            "message": "s",
            "channel": "q"
          }
        },
        "EmergencyBroadcast": {
          "arg": {
            "message": "s",
            "dict": "a{sv}"
          }
        }
      }
    },
    "org.ofono.ConnectionManager": {
      "method": {
        "GetProperties": {
          "res": {
            "properties": "a{sv}"
          }
        },
        "SetProperty": {
          "arg": {
            "property": "s",
            "value": "v"
          }
        },
        "AddContext": {
          "arg": {
            "type": "s"
          },
          "res": {
            "path": "o"
          }
        },
        "RemoveContext": {
          "arg": {
            "path": "o"
          }
        },
        "DeactivateAll": {},
        "GetContexts": {
          "res": {
            "contexts_with_properties": "a(oa{sv})"
          }
        }
      },
      "signal": {
        "PropertyChanged": {
          "arg": {
            "name": "s",
            "value": "v"
          }
        },
        "ContextAdded": {
          "arg": {
            "path": "o",
            "properties": "v"
          }
        },
        "ContextRemoved": {
          "arg": {
            "path": "o"
          }
        }
      }
    },
    "org.ofono.MessageManager": {
      "method": {
        "GetProperties": {
          "res": {
            "properties": "a{sv}"
          }
        },
        "SetProperty": {
          "arg": {
            "property": "s",
            "value": "v"
          }
        },
        "SendMessage": {
          "arg": {
            "to": "s",
            "text": "s"
          },
          "res": {
            "path": "o"
          }
        },
        "GetMessages": {
          "res": {
            "messages": "a(oa{sv})"
          }
        }
      },
      "signal": {
        "PropertyChanged": {
          "arg": {
            "name": "s",
            "value": "v"
          }
        },
        "IncomingMessage": {
          "arg": {
            "message": "s",
            "info": "a{sv}"
          }
        },
        "ImmediateMessage": {
          "arg": {
            "message": "s",
            "info": "a{sv}"
          }
        },
        "MessageAdded": {
          "arg": {
            "path": "o",
            "properties": "a{sv}"
          }
        },
        "MessageRemoved": {
          "arg": {
            "path": "o"
          }
        }
      }
    },
    "org.ofono.PushNotification": {
      "method": {
        "RegisterAgent": {
          "arg": {
            "path": "o"
          }
        },
        "UnregisterAgent": {
          "arg": {
            "path": "o"
          }
        }
      }
    },
    "org.ofono.SmartMessaging": {
      "method": {
        "RegisterAgent": {
          "arg": {
            "path": "o"
          }
        },
        "UnregisterAgent": {
          "arg": {
            "path": "o"
          }
        },
        "SendBusinessCard": {
          "arg": {
            "to": "s",
            "card": "ay"
          },
          "res": {
            "path": "o"
          }
        },
        "SendAppointment": {
          "arg": {
            "to": "s",
            "appointment": "ay"
          },
          "res": {
            "path": "o"
          }
        }
      }
    }
  },
  "context2": {},
  "operator": {}
}
