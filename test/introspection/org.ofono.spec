{
  "": {
    "org.freedesktop.DBus.Introspectable": {
      "method": {
        "Introspect": {
          "res": {
            "xml": "s"
          }
        }
      }
    },
    "org.ofono.Manager": {
      "method": {
        "GetModems": {
          "res": {
            "modems": "a(oa{sv})"
          }
        }
      },
      "signal": {
        "ModemAdded": {
          "arg": {
            "path": "o",
            "properties": "a{sv}"
          }
        },
        "ModemRemoved": {
          "arg": {
            "path": "o"
          }
        }
      }
    }
  },
  "huawei_0": {},
  "zte_0": {},
  "huawei_1": {},
  "simcom_0": {}
}
